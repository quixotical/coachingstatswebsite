<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StatsCategory', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('teams_id')->unsigned();
            $table->foreign('teams_id')->references('id')->on('Teams');
            $table->string('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('StatsCategory');
    }
}
