<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMemberStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TeamMemberStats', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('teamMember_id')->unsigned();
            $table->foreign('teamMember_id')->references('id')->on('TeamMembers');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('StatsCategory');
            $table->integer('score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TeamMemberStats');
    }
}
