<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TeamMembers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('teams_id')->unsigned();
            $table->foreign('teams_id')->references('id')->on('Teams')->onDelete('cascade');
            $table->string('teamMemberName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TeamMembers');
    }
}
