<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMemberStats extends Model
{
    protected $fillable = [
        'teamMember_id', 'category_id', 'score'
    ];

    protected $guarded = [
        'created_at','updated_at'
    ];
}
