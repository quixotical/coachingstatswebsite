<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    public function authenticate(Request $request){
        //get credentials from request

        $credentials = $request->only('email','password');
        try{
            //attempt to verify credentials
            if(! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
            }catch (JWTException $e){
                // something went wrong whilst attempting to encode the token
                return response()->json(['error' => 'could not create token'], 500);
            }

            //all good so return the token
            return response()->json(compact('token'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|static
     */
    public function postRegister(Request $request){
        //get user email and password from app
        $email = $request->only('email');
        $password = $request->only('password');

        //check if user already exists
        $userExists = User::where('email', $email['email'])->first();

        if (count($userExists) > 0) {
            return response()->json(['error' => 'user already exists'], 401);
            //if user does not exist, then attempt to create a new user
        } else {
            $user = User::create([
                'email' => $email['email'],
                'password' => bcrypt($password['password'])
            ]);

            $credentials = $request->only('email', 'password');
            try {
                //get a auth token for the user if their credentials check out
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong whilst attempting to encode the token
                return response()->json(['error' => 'could not create token'], 500);
            }

            //all good so return the token as a part of the user object
            $data['user'] = $user;
            $data['token'] = $token;
            return response()->json(['data' => $data]);
        }
    }
    public function postLogin(Request $request){
        $email = $request->only('email');
        $password = $request->only('password');

        $user = User::where('email', $email['email'])->first();
        if(count($user) > 0){
            if(password_verify($password['password'], $user->password)){
                $credentials = $request->only('email','password');
                try{
                    //attempt to verify credentials
                    if(! $token = JWTAuth::attempt($credentials)) {
                        return response()->json(['error' => 'invalid_credentials',
                                                 'reason' => 'Verify Credentials failed'], 403);
                    }
                }catch (JWTException $e){
                    // something went wrong whilst attempting to encode the token
                    return response()->json(['error' => 'could not create token'], 500);
                }
                $data['user'] = $user;
                $data['token'] = $token;
                return response()->json(['data' => $data]);
            }else{
                return response()->json(['error' => 'Invalid username or password',
                                         'reason' => 'Invalid Password'], 401);
            }
        }else{
            return response()->json(['error' => 'Invalid username or password',
                                     'reason' => 'Email not found'], 401);
        }
    }
}
