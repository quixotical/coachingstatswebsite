<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AuthRequiredController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth');
    }
}
