<?php

namespace App\Http\Controllers;

use App\TeamMemberStats;
use Illuminate\Http\Request;

use App\Http\Requests;

class TeamMemberStatsController extends AuthRequiredController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request - The request for posting a new team member score is as follows:
     *
     *  {
     *   "teamMember_id":"3",
     *    "category_score":[{"category_id":"1", "score":"8"}, {"category_id":"4", "score":"5"}]
     *   }
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category_scores = $request->only('category_score');
        $teamMember_id = $request->only('teamMember_id');
        $newStats = [];
        foreach ($category_scores['category_score'] as $category_score){
            $newUserStats = TeamMemberStats::create([
                'teamMember_id' => $teamMember_id['teamMember_id'],
                'category_id' => $category_score['category_id'],
                'score' => $category_score['score']
            ]);
            array_push($newStats, $newUserStats);
        }

        return response()->json(['newStats'=>$newStats]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
