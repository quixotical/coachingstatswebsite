<?php

namespace App\Http\Controllers;

use App\TeamMember;
use Illuminate\Http\Request;

use App\Http\Requests;

class TeamMembersController extends AuthRequiredController
{
    /**
     * Get the teamMembers for  a given user
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Get page to add a new team Member on web
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created teamMember.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teamMemberName = $request->only('teamMemberName');
        $teamId = $request->only('teamId');

        $teamMember = TeamMember::create([
            'teams_id' => $teamId['teamId'],
            'teamMemberName' => $teamMemberName['teamMemberName']
        ]);

        return response()->json(['teamMember' => $teamMember]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return response()->json(['success' => 'we did it'], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
