<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function($api){
    $api->get('/', function(){
        return view('welcome');
    });

    $api->get('/users', 'App\Http\Controllers\UserController@index');
    //route to register a new user
    $api->post('register', 'App\Http\Controllers\Auth\AuthController@postRegister');

    //route to log in an existing user
    $api->post('login', 'App\Http\Controllers\Auth\AuthController@postLogin');

    $api->get('authenticate', 'App\Http\Controllers\Auth\AuthController@authenticate');
    $api->post('authenticate', 'App\Http\Controllers\Auth\AuthController@authenticate');

    //routes to teams
    $api->post('postTeam', 'App\Http\Controllers\TeamsController@store');
    $api->get('getTeams', 'App\Http\Controllers\TeamsController@show');
    //routes to team members
    $api->post('postTeamMember', 'App\Http\Controllers\TeamMembersController@store');

    //routes to Stats Categories
    $api->post('postStatsCategory', 'App\Http\Controllers\StatsCategoryController@store');

    //routes to Team Members Stats
    $api->post('postTeamMemberStats', 'App\Http\Controllers\TeamMemberStatsController@store');
});