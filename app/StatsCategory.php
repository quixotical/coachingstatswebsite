<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsCategory extends Model
{
    protected $fillable = [
        'teams_id', 'category'
    ];

    protected $guarded = [
        'created_at', 'updated_at'
    ];
}
