<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The attributes that can be mass assigned
     * @var array
     */
    protected $fillable = [
      'teamName', 'sport', 'user_email'
    ];

    /**
     * the attribues that are protected and will be hidden
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];
}
