<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    protected $fillable = [
        'teamMemberName', 'teams_id'
    ];

    protected $guarded = [
        'created_at', 'updated_at', 'id'
    ];
}
